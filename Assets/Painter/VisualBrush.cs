﻿using UnityEngine;

[ExecuteInEditMode]
public class VisualBrush : MonoBehaviour {

    public LineRenderer LineNormal;
    public Transform Circle;

    public BrushSetting Setting { get; private set; }

    public Mesh Mesh { get; private set; }

    public void Set(BrushSetting setting)
    {
        Setting = setting;
    }

    public float Radius
    {
        get
        {
            if(Setting == null)
            {
                return 1.0f;
            }
            return Setting.Radius;
        }
    }

    public void OnRenderObject()
    {
        if (!LineNormal.sharedMaterial || !Mesh)
            return;
        LineNormal.sharedMaterial.SetPass(0);
        Sculptor.DrawTriangle(Mesh.vertices, Mesh.triangles, transform.localPosition, Radius);
    }

    public void SetMesh(Mesh mesh)
    {
        Mesh = mesh;
    }

    public void Refresh(Ray ray)
    {
        transform.localPosition = ray.origin;
        transform.localRotation = Quaternion.FromToRotation(Vector3.forward, -ray.direction);
        Circle.localScale = Vector3.one * Radius * 2.0f;
    }
}
