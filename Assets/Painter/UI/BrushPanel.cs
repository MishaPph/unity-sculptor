﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrushPanel : MonoBehaviour {
    [SerializeField] private GameObject _brushPrefab;
    [SerializeField] private Transform _content;
    [SerializeField] private ToggleGroup _toggleGroup;

    private List<BrushItem> _items = new List<BrushItem>();

    public event Action<BrushSetting> OnSelectSetting;

    #region BrushPanel
    [SerializeField] private Slider _radius;
    [SerializeField] private Slider _strength;

    private BrushItem _active;
    #endregion

    public void InitBrushes(BrushSetting[] settings)
    {
        foreach(var data in settings)
        {
            var go = Instantiate(_brushPrefab);
            go.transform.SetParent(_content, true);
            var item = go.GetComponent<BrushItem>();
            item.Set(data);
            item.Toggle.onValueChanged.AddListener(OnSelect);
            item.Toggle.group = _toggleGroup;
            _items.Add(item);
        }
        _active = _items[0];
        _active.Toggle.isOn = true; // select first

       // ShowSettingsPanel(_active.Data);

        _radius.onValueChanged.AddListener(OnChangeRadius);
        _strength.onValueChanged.AddListener(OnChangeStrength);
    }

    private void OnChangeStrength(float value)
    {
        _active.Data.Strength = value;
    }

    private void OnChangeRadius(float value)
    {
        _active.Data.Radius = value;
    }

    private void OnSelect(bool isOn)
    {
        foreach (var item in _items)
        {
            if (item.Toggle.isOn)
            {
                _active = item;
                ShowSettingsPanel(item.Data);
                if (OnSelectSetting != null) {
                    OnSelectSetting(item.Data);
                }
            }
        }
    }

    private void ShowSettingsPanel(BrushSetting data)
    {
        _strength.value = data.Strength;
        _radius.value = data.Radius;
    }

}
