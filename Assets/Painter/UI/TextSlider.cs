﻿using UnityEngine;
using UnityEngine.UI;

public class TextSlider : MonoBehaviour {
    public Slider Slider;
    public Text From;
    public Text To;
    public Text Value;
    
    public void Awake()
    {
        From.text = Slider.minValue.ToString();
        To.text = Slider.maxValue.ToString();
        Value.text = Slider.value.ToString();
        Slider.onValueChanged.AddListener(OnValueChanged);
    }

    private void OnValueChanged(float arg0)
    {
        Value.text = Slider.value.ToString();
    }
}
