﻿using UnityEngine;
using UnityEngine.UI;

public class BrushItem : MonoBehaviour {
    [SerializeField] private Text _name;

    public Toggle Toggle;

    public BrushSetting Data { get; private set; }

    public void Set(BrushSetting data)
    {
        Data = data;
        _name.text = Data.name;
    }
}
