﻿using System.Collections.Generic;
using UnityEngine;

public class Sculptor : MonoBehaviour {
    [SerializeField] private MeshFilter _target;
    [SerializeField] private VisualBrush _visualBrush;
    [SerializeField] private BrushSetting brush;
    private Ray ray;
    List<int> _selected = new List<int>();

    private int[] cache_triangles;

    public Material lineMaterial;
    [SerializeField] private BrushPanel _panel;
    [SerializeField] private BrushSetting[] AvailableBrushes;

    private void Start()
    {
        _visualBrush = Instantiate(Resources.Load("VisualBrush") as GameObject).GetComponent<VisualBrush>();
        _panel.OnSelectSetting += OnSelectBrush;
        _panel.InitBrushes(AvailableBrushes);
        _visualBrush.SetMesh(_target.mesh);
        cache_triangles = _target.mesh.triangles;
    }
    
    private void OnSelectBrush(BrushSetting setting)
    {
        brush = setting;
        _visualBrush.Set(brush);
    }

    private void Update()
    {
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
      
        if (!MeshHelper.WorldRaycast(mouseRay, _target.transform, _target.mesh, cache_triangles, out ray))
        {
            _visualBrush.gameObject.SetActive(false);
            return;
        }
        _visualBrush.gameObject.SetActive(true);
        _visualBrush.Refresh(ray);

        MeshHelper.SelectedVertices(_target.mesh.vertices, _target.mesh.normals, cache_triangles, ray.origin, brush.Radius, _selected);

        if (Input.GetMouseButtonDown(0))
        {
            MeshHelper.ApplyToMesh(_target.mesh, ray, _selected, brush);
        } else if(Input.GetMouseButtonUp(0)) {
            _selected.Clear();
        } else if(Input.GetMouseButton(0)) {
            MeshHelper.ApplyToMesh(_target.mesh, ray, _selected, brush);
        }
    }

    public static void DrawTriangle(Vector3[] vertices, int[] triangles, Vector3 fromPoint, float radius)
    {
        GL.Begin(GL.LINES);
        for (int i = 0; i + 2 < triangles.Length; i += 3)
        {
            Vector3 vec1 = vertices[triangles[i]];
            Vector3 vec2 = vertices[triangles[i + 1]];
            Vector3 vec3 = vertices[triangles[i + 2]];

            var d1 = Vector3.Distance(vec1, fromPoint);
            var d2 = Vector3.Distance(vec2, fromPoint);
            var d3 = Vector3.Distance(vec3, fromPoint);

            if (d1 > radius && d2 > radius && d3 > radius)
            {
                continue;
            }

            GL.Color(Color.Lerp(Color.red, Color.white, (d1 + d2 + d3) / (3 * radius)));

            GL.Vertex(vec1);
            GL.Vertex(vec2);

            GL.Vertex(vec2);
            GL.Vertex(vec3);

            GL.Vertex(vec3);
            GL.Vertex(vec1);
        }
        GL.End();
    }

}
