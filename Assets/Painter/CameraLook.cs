﻿using UnityEngine;

public class CameraLook : MonoBehaviour {
    [SerializeField] private float _speed = 0.5f;

	private void LateUpdate() {
        if (Input.GetMouseButton(1))
        {
            transform.Translate(-Vector3.right * Input.GetAxis("Mouse X")* _speed);
            transform.Translate(-Vector3.up * Input.GetAxis("Mouse Y") * _speed);
            transform.LookAt(Vector3.zero);
        }

        transform.localPosition += transform.forward * Input.GetAxis("Mouse ScrollWheel");
    }
}
