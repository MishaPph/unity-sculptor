﻿using System.Collections.Generic;
using UnityEngine;

public static class  MeshHelper {

    public static Mesh DeepCopy(Mesh src)
    {
        Mesh dest = new Mesh();
        Copy(dest, src);
        return dest;
    }

    public static bool WorldRaycast(Ray InWorldRay, Transform transform, Mesh mesh, int[] triangles, out Ray hit)
    {
        InWorldRay = transform.InverseTransformRay(InWorldRay);
        return MeshRaycast(InWorldRay, mesh, triangles, out hit);
    }

    public static bool MeshRaycast(Ray inRay, Mesh mesh, int[] triangles, out Ray hit)
    {
        		// vars used in loop
        float hitDistance = Mathf.Infinity;
        Vector3 hitPoint = new Vector3(0f, 0f, 0f);    // vars used in loop
        Vector3 a, b, c;
        int hitFace = -1;
        Vector3 o = inRay.origin, d = inRay.direction;
        Vector3 normal = Vector3.zero;

        Vector3[] vertices = mesh.vertices;

        for (var curTri = 0; curTri < triangles.Length; curTri += 3)
        {
            a = vertices[triangles[curTri + 0]];
            b = vertices[triangles[curTri + 1]];
            c = vertices[triangles[curTri + 2]];

            if (Math.RayIntersectsTriangle2(inRay, a, b, c, ref hitDistance))
            {
                hitFace = curTri / 3;
                var p1 = mesh.normals[triangles[curTri + 0]];
                var p2 = mesh.normals[triangles[curTri + 1]];
                var p3 = mesh.normals[triangles[curTri + 2]];
                normal = ((p1 + p2 + p3) / 3);
                break;
            }
        }
        hit = new Ray(inRay.GetPoint(hitDistance), normal);
        return hitFace > -1;
    }

    public static void Copy(Mesh dest, Mesh src)
    {
        dest.Clear();
        dest.vertices = src.vertices;

        List<Vector4> uvs = new List<Vector4>();

        src.GetUVs(0, uvs); dest.SetUVs(0, uvs);
        src.GetUVs(1, uvs); dest.SetUVs(1, uvs);
        src.GetUVs(2, uvs); dest.SetUVs(2, uvs);
        src.GetUVs(3, uvs); dest.SetUVs(3, uvs);

        dest.normals = src.normals;
        dest.tangents = src.tangents;
        dest.boneWeights = src.boneWeights;
        dest.colors = src.colors;
        dest.colors32 = src.colors32;
        dest.bindposes = src.bindposes;

        dest.subMeshCount = src.subMeshCount;

        for (int i = 0; i < src.subMeshCount; i++)
            dest.SetIndices(src.GetIndices(i), src.GetTopology(i), i);
    }

    public static void SelectedVertices(Vector3[] vertices, Vector3[] normals, int[] triangles, Vector3 position, float range, List<int> selected)
    {
        selected.Clear();
        for (int i = 0; i + 2 < triangles.Length; i += 3)
        {
            if (!InRange(vertices[triangles[i]], position, range) &&
                !InRange(vertices[triangles[i + 1]], position, range) &&
                !InRange(vertices[triangles[i + 2]], position, range))
            {
                continue;
            }
            selected.Add(triangles[i]);
            selected.Add(triangles[i + 1]);
            selected.Add(triangles[i + 2]);
        }
        return;
    }

    public static void ApplyToMesh(Mesh mesh, Ray ray, List<int> vertexIndex, BrushSetting brush)
    {
        var vertices = mesh.vertices;
        foreach (var curTri in vertexIndex)
        {
            vertices[curTri] += ray.direction * brush.GetWeight(Vector3.Distance(vertices[curTri], ray.origin));
        }
        mesh.vertices = vertices;
    }

    private static bool InRange(Vector3 a, Vector3 b, float range)
    {
        return (a - b).magnitude < range;
    }
}
