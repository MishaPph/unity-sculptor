using UnityEngine;

public static class Math
{
    static Vector3 tv1, tv2, tv3, tv4;

    public static bool RayIntersectsTriangle2(Ray ray, Vector3 vert0, Vector3 vert1,Vector3 vert2, ref float distance) {
        float det;

        Unbox_Vector.Subtract(vert0, vert1, ref tv1);
        Unbox_Vector.Subtract(vert0, vert2, ref tv2);

        Unbox_Vector.Cross(ray.direction, tv2, ref tv4);
        det = Vector3.Dot(tv1, tv4);

        if (det < Mathf.Epsilon)
            return false;

        Unbox_Vector.Subtract(vert0, ray.origin, ref tv3);

        float u = Vector3.Dot(tv3, tv4);

        if (u < 0f || u > det)
            return false;

        Unbox_Vector.Cross(tv3, tv1, ref tv4);

        float v = Vector3.Dot(ray.direction, tv4);

        if (v < 0f || u + v > det)
            return false;

        distance = Vector3.Dot(tv2, tv4) * (1f / det);

        return true;
    }

}

