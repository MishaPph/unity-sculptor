﻿using UnityEngine;

public static class TransformExt
{
    public static Ray InverseTransformRay(this Transform transform, Ray InWorldRay)
    {
        Vector3 o = InWorldRay.origin;
        o -= transform.position;
        o = transform.worldToLocalMatrix * o;
        Vector3 d = transform.worldToLocalMatrix.MultiplyVector(InWorldRay.direction);
        return new Ray(o, d);
    }
}