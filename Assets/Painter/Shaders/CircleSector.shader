Shader "CircleSector" {
Properties {
    _Color ("Color", Color) = (0.3,1,0.3,0.3)
	_Min ("Min", Range(0.0, 1.0)) = 0.0
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Blend SrcAlpha OneMinusSrcAlpha 
	
	Cull Off Lighting Off 
	ZWrite Off
    ZTest Always
	Fog { Mode Off}

	
	SubShader {
		Pass {
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_particles

			#include "UnityCG.cginc"

			fixed4 _Color;
			float _Value;
			float _Min;

			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.texcoord = v.texcoord;
				return o;
			}

          
			fixed4 frag (v2f i) : COLOR
			{
			    float posX = (i.texcoord.x - 0.5);
                float posY = (i.texcoord.y - 0.5);
                
                float c = sqrt(posX*posX + posY*posY);

                if(c > 0.5f || c < _Min*0.5f)
                    discard;

				return _Color;
			}
			ENDCG 
		}
	}
}
}
