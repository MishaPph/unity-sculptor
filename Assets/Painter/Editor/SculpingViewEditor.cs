﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class SculpingViewEditor : EditorWindow {

    private PreviewRenderUtility _previewRender;

    private List<BrushSetting> _availableBrushes = null;
    private string[] _availableBrushes_str = null;
    private int indexBrush = 0;

    private  VisualBrush _visualBrush;
    public VisualBrush VisualBrush
    {
        get
        {
            if (!_visualBrush)
            {
                if(_previewRender == null)
                {
                    InitPreview();
                }
                _visualBrush = _previewRender.InstantiatePrefabInScene(Resources.Load("VisualBrush") as GameObject).GetComponent<VisualBrush>();
            }
            return _visualBrush;
        }
    }

    private BrushSetting SelectedBrush
    {
        get
        {
            if (_availableBrushes == null || _availableBrushes.Count == 0)
            {
                InitBurshes();
            }
            return _availableBrushes[indexBrush];
        }
    }

    private Ray ray;

    public int[] CacheTriangle { get; private set; }

    private MeshRenderer meshRender;

    readonly List<int> _selectedVerticles = new List<int>();

    public Mesh Mesh;

    private void InitPreview()
    {
        _previewRender = new PreviewRenderUtility();
        _previewRender.camera.clearFlags = CameraClearFlags.SolidColor;
        _previewRender.camera.transform.position = new Vector3(0, 0, -10);// = Camera.main;
    }

    public void Initialize()
    {
        InitPreview();
        InitBurshes();
    }

    private void InitBurshes()
    {
        _availableBrushes = Resources.FindObjectsOfTypeAll<BrushSetting>().Where(x => !string.IsNullOrEmpty(AssetDatabase.GetAssetPath(x))).ToList();

        if (_availableBrushes.Count < 1)
            _availableBrushes.Add(BrushSetting.CreateDefault());
        indexBrush = 0;
        _availableBrushes_str = new string[_availableBrushes.Count];
        for(var i = 0; i < _availableBrushes.Count; i++)
        {
            _availableBrushes_str[i] = _availableBrushes[i].name;
        }
        VisualBrush.Set(SelectedBrush);
    }

    [MenuItem("Tools/Sculptor")]
    private static void InitializeWindow()
    {
        var window = GetWindow<SculpingViewEditor>("Sculptor", true);
        window.titleContent.tooltip = "Sculptor";
        window.autoRepaintOnSceneChange = true;
        window.Show();
    }

    private bool TrySelectObject()
    {
        var selectGo = Selection.activeGameObject;

        if (selectGo == null)
        {
            Mesh = null;
            if (Mesh != null)
                return true;
            EditorGUILayout.LabelField("No game object selected.");
            return false;
        }

        var fM = selectGo.GetComponent<MeshFilter>();
        var mR = selectGo.GetComponent<MeshRenderer>();
        if (fM == null || mR == null)
        {
            if (Mesh != null)
                return true;
            EditorGUILayout.LabelField("Gameobject does not contains the required components.");
            return false;
        }
        SetSelect(selectGo);
        return Mesh != null;
    }

    private void SetSelect(GameObject go)
    {
        meshRender = go.GetComponent<MeshRenderer>();
        Mesh = MeshHelper.DeepCopy(go.GetComponent<MeshFilter>().sharedMesh);
        go.GetComponent<MeshFilter>().mesh = Mesh;
        CacheTriangle = Mesh.triangles;
        VisualBrush.SetMesh(Mesh);
    }

    public void OnGUI()
    {
        if (!TrySelectObject())
        {
            return;
        }

        if (_previewRender == null)
        {
            Initialize();
        }

        Draw(Mesh, meshRender.sharedMaterial);
        if (SelectedBrush)
        {
            DrawUI();
        }
     
        GetInput();
    }

    private void DrawUI()
    {
        EditorGUILayout.BeginVertical("box", GUILayout.MaxWidth(162));
        EditorGUILayout.LabelField("Selected: " + meshRender.name);

        EditorGUI.BeginChangeCheck();
            indexBrush = EditorGUILayout.Popup(indexBrush, _availableBrushes_str, "Popup");

        if (EditorGUI.EndChangeCheck())
        {
            VisualBrush.Set(SelectedBrush);
        }
        DrawBrushSettings(SelectedBrush);
    }

    private void DrawBrushSettings(BrushSetting brushSetting)
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Radius", GUILayout.MaxWidth(48));
        brushSetting.Radius = EditorGUILayout.Slider(brushSetting.Radius, 0.01f, 1.0f);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Strength", GUILayout.MaxWidth(48));
        brushSetting.Strength = EditorGUILayout.Slider(brushSetting.Strength, 0.001f, 0.4f);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
    }

    private void Draw(Mesh  mesh, Material mtl)
    {
        var rect = new Rect(0, 0, position.width, position.height);
        _previewRender.BeginPreview(rect, GUIStyle.none);
        _previewRender.DrawMesh(mesh, Matrix4x4.identity, mtl, 0);
        //VisualBrush.OnRenderObject();
        _previewRender.camera.Render();
        var r = _previewRender.EndPreview();
        GUI.DrawTexture(new Rect(0, position.height, position.width, -position.height), r);
    }

    private void Update()
    {
        Repaint();
    }

    private void GetInput()
    {
        Transform camTransform = _previewRender.camera.transform;
        Event e = Event.current;
        if(e.button == 0)
        {
            BrushOnObject(new Vector2(e.mousePosition.x, e.mousePosition.y));
            if(e.type == EventType.MouseDrag || e.type == EventType.MouseDown)
            {
                MeshHelper.SelectedVertices(Mesh.vertices, Mesh.normals, CacheTriangle, ray.origin, SelectedBrush.Radius, _selectedVerticles);
                MeshHelper.ApplyToMesh(Mesh, ray, _selectedVerticles, SelectedBrush);
                Draw(Mesh, meshRender.sharedMaterial);
            }
        }
        else if (e.button == 1)
        {
            UnpdateCamera(camTransform, e.delta);
            Repaint();
        } 
        else if (e.isScrollWheel) {
            camTransform.localPosition += camTransform.forward * e.delta.y * 0.01f;
            Repaint();
        }
    }

    private bool BrushOnObject(Vector2 point)
    {
        Ray mouseRay = _previewRender.camera.ScreenPointToRay(point);
        var mesh = Mesh;
        if (!MeshHelper.WorldRaycast(mouseRay, meshRender.transform, mesh, mesh.triangles, out ray))
        {
            VisualBrush.gameObject.SetActive(false);
            return false;
        }
        VisualBrush.gameObject.SetActive(true);
        VisualBrush.Refresh(ray);
        return true;
    }

    private void UnpdateCamera(Transform tr, Vector2 delta)
    {
        var d = tr.localPosition.magnitude;
        tr.Translate(-Vector3.right * delta.x * 0.01f - Vector3.up * delta.y * 0.01f);
        tr.LookAt(Vector3.zero);
        tr.localPosition = tr.localPosition.normalized * d;
    }
}
