﻿using UnityEngine;

public class Wireframe : MonoBehaviour {
    public Color lineColor = new Color(0.0f, 1.0f, 1.0f);
    public Material lineMaterial;
    Mesh mesh;

    private void Start()
    {
        mesh = gameObject.GetComponent<MeshFilter>().mesh;
    }

    private Vector3 ToWorld(Vector3 vec)
    {
        return transform.TransformPoint(vec);
    }

    private void OnRenderObject()
    {
        lineMaterial.color = lineColor;
        lineMaterial.SetPass(0);
        GL.Begin(GL.LINES);

        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;

        for (int i = 0; i + 2 < triangles.Length; i += 3)
        {
            Vector3 vec1 = ToWorld(vertices[triangles[i]]);
            Vector3 vec2 = ToWorld(vertices[triangles[i + 1]]);
            Vector3 vec3 = ToWorld(vertices[triangles[i + 2]]);

            GL.Vertex(vec1);
            GL.Vertex(vec2);
            GL.Vertex(vec2);
            GL.Vertex(vec3);
            GL.Vertex(vec3);
            GL.Vertex(vec1);
        }
        GL.End();
    }
}