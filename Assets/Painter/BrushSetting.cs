﻿using System.IO;
using UnityEditor;
using UnityEngine;

public class BrushSetting : ScriptableObject
{
    [SerializeField, Range(0.001f, 1.0f)] public float Radius = 0.1f;

    [Range(0.001f, 0.1f)] public float Strength = 0.1f;

    public AnimationCurve Curve = new AnimationCurve(new Keyframe(0f, 1f),  new Keyframe(1f, 0f));

    public virtual  float GetWeight(float distance)
    {
        return Curve.Evaluate(distance / Radius)* Strength;
    }

    public static BrushSetting CreateDefault()
    {
        string full = "Assets/Painter/Resources/" + "Default.asset";

        BrushSetting asset = AssetDatabase.LoadAssetAtPath<BrushSetting>(full);

        if (asset == null)
        {
            asset = CreateInstance<BrushSetting>();
            EditorUtility.SetDirty(asset);
            string folder = Path.GetDirectoryName(full);

            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            AssetDatabase.CreateAsset(asset, full);
        }
        return asset;
    }
}
